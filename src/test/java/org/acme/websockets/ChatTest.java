package org.acme.websockets;

import java.net.URI;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.websocket.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.acme.websockets.model.ChatMessage;
import org.acme.websockets.model.MessageType;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;

import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ChatTest {

    private static final LinkedBlockingDeque<String> MESSAGES = new LinkedBlockingDeque<>();

    @TestHTTPResource("/chat/stu")
    URI uri;

    @Inject
    private  ChatMessageRepository chatMessageRepository;

    @Inject
    ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        chatMessageRepository.deleteAll();
    }

    @Test
    @Order(1)
    public void testWebsocketChat() throws Exception {
        try (Session session = ContainerProvider.getWebSocketContainer().connectToServer(Client.class, uri)) {
            Assertions.assertEquals("CONNECT", MESSAGES.poll(10, TimeUnit.SECONDS));
            String result1 =  MESSAGES.poll(10, TimeUnit.SECONDS);
            Assertions.assertNotNull(result1);
            ChatMessage cm = objectMapper.readValue(result1, ChatMessage.class);
            Assertions.assertEquals(cm.getMessageType(), MessageType.SYSTEM);
            Assertions.assertEquals(cm.getUsername(), "stu");
            Assertions.assertEquals(cm.getMessage(), "joined");

            session.getAsyncRemote().sendText("hello world");
            String result2 =  MESSAGES.poll(10, TimeUnit.SECONDS);
            Assertions.assertNotNull(result1);
            ChatMessage cm2 = objectMapper.readValue(result2, ChatMessage.class);
            Assertions.assertEquals(cm2.getMessageType(), MessageType.USER);
            Assertions.assertEquals(cm2.getUsername(), "stu");
            Assertions.assertEquals(cm2.getMessage(), "hello world");

            Assertions.assertEquals(2, chatMessageRepository.getAllMessages().size());
            Assertions.assertEquals("hello world", chatMessageRepository.findByUser("stu").get(0).getMessage());
        }
    }

    @Test
    @Order(2)
    public void testMessageWrongLenght() throws Exception {
        try (Session session = ContainerProvider.getWebSocketContainer().connectToServer(Client.class, uri)) {
            String s = RandomStringUtils.random(301, true, true);
            Assertions.assertEquals("CONNECT", MESSAGES.poll(10, TimeUnit.SECONDS));

            String result1 =  MESSAGES.poll(10, TimeUnit.SECONDS);
            Assertions.assertNotNull(result1);

            session.getAsyncRemote().sendText(s);
            Assertions.assertEquals(null, MESSAGES.poll(10, TimeUnit.SECONDS));
            Assertions.assertEquals(1, chatMessageRepository.getAllMessages().size());
        }
    }


    @ClientEndpoint
    public static class Client {

        @OnOpen
        public void open(Session session) {
            MESSAGES.add("CONNECT");
            // Send a message to indicate that we are ready,
            // as the message handler may not be registered immediately after this callback.
            session.getAsyncRemote().sendText("_ready_");
        }

        @OnMessage
        void message(String msg) {
            MESSAGES.add(msg);
        }

    }

}
