package org.acme.websockets;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Page;
import org.acme.websockets.model.ChatMessage;
import org.acme.websockets.model.MessageType;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;


@ApplicationScoped
public class ChatMessageRepository implements PanacheMongoRepository<ChatMessage> {

    public List<ChatMessage> findByUser(String username) {
        return find("{'username': ?1, 'messageType': ?2 }", username,  MessageType.USER).page(Page.ofSize(1000)).list();
    }

    public List<ChatMessage> getAllMessages() {
        return ChatMessage.listAll();
    }

    @Valid
    public ChatMessage add(String username, String message, MessageType messageType) {
        ChatMessage wsmessage =  new ChatMessage();
        wsmessage.setUsername(username);
        wsmessage.setTimestamp(LocalDateTime.now());
        wsmessage.setMessage(message);
        wsmessage.setMessageType(messageType);
        wsmessage.persist();
        return wsmessage;
    }

}
