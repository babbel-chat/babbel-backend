package org.acme.websockets.model;

public enum MessageType {
    SYSTEM,
    USER
}
