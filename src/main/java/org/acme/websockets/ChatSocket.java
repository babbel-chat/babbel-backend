package org.acme.websockets;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.acme.websockets.model.ChatMessage;
import org.acme.websockets.model.MessageType;
import org.jboss.logging.Logger;

@ServerEndpoint("/chat/{username}")
@ApplicationScoped
public class ChatSocket {

    @Inject
    ChatMessageRepository messageRepository;

    private static final Logger LOG = Logger.getLogger(ChatSocket.class);

    Map<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        sessions.put(username, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("username") String username) {
        sessions.remove(username);
        writeToDbAndSendAsJsonStr(username, "left", MessageType.SYSTEM);
    }

    @OnError
    public void onError(Session session, @PathParam("username") String username, Throwable throwable) {
        sessions.remove(username);
        LOG.error("onError", throwable);
        String message = "left on error: " + throwable;
        writeToDbAndSendAsJsonStr(username, message, MessageType.SYSTEM);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("username") String username) {
        if(message.length() > 300 || message.isEmpty() || username.isEmpty()) {
            LOG.warn("WARN: Message out of bounds or missing username");
            return;
        }
        if (message.equalsIgnoreCase("_ready_")) {
            writeToDbAndSendAsJsonStr(username, "joined", MessageType.SYSTEM);
        } else {
            writeToDbAndSendAsJsonStr(username, message, MessageType.USER);
        }
    }

    private void writeToDbAndSendAsJsonStr(String username, String message, MessageType messageType) {
        String jsonStr = "";
        ChatMessage cm = messageRepository.add(username, message, messageType);
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonStr = mapper.writeValueAsString(cm);
        } catch (JsonProcessingException e) {
            LOG.error("onError", e);
        }
        broadcast(jsonStr);
    }

    private void broadcast(String message) {
        sessions.values().forEach(s -> {
            s.getAsyncRemote().sendObject(message, result -> {
                if (result.getException() != null) {
                    LOG.error("Unable to send message: " + result.getException());
                }
            });
        });
    }

}