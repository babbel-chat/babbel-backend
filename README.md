# Babel - Backend

## Requiements
 The implentation is realized on [Quarkus](https://quarkus.io/) using Websocket technology.


 To start the application you need to follow the instructions [Get Started - Quarkus](https://quarkus.io/get-started/)

 Since all the data from the chat is persisted in the MongoDB database, then MongoDB should start as well on local machine.
 > _docker run -ti --rm -p 27017:27017 mongo:4.0_

## Development server
 Run 
 > _mvn clean install quarkus:dev_ 
 
 to start the application on http://localhost:8080/

